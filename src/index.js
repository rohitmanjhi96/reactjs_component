import React from 'react';
import ReactDOM from 'react-dom';
import CommentDetails from './CommentDetails'
import faker from 'faker';
import ApprovalCard from './ApprovalCard';

const App = () => {
  return (
      <div className="ui comments">
      <ApprovalCard >
        <CommentDetails author="sam" 
            timeAgo = "Today at 4:45PM" 
            content = "I am good" 
            avatar = {faker.image.avatar()}
        />
      </ApprovalCard>
      
      <ApprovalCard >
       <CommentDetails 
         author="Rohit" 
         timeAgo = "Today at 4:45PM" 
         content="I am good" 
         avatar = {faker.image.avatar()}
        />
      </ApprovalCard>
      
      <ApprovalCard>
       <CommentDetails author="Alexa" timeAgo = "Today at 4:45PM" content="I am good" avatar = {faker.image.avatar()} />
      </ApprovalCard>
      
      <ApprovalCard>
       <CommentDetails author="Priyanka" timeAgo = "Today at 4:45PM" content="I am good" avatar = {faker.image.avatar()} />
      </ApprovalCard>
      
      <ApprovalCard>
       <CommentDetails author="kangna" timeAgo = "Today at 4:45PM" content="I am good" avatar = {faker.image.avatar()}/>
      </ApprovalCard>
      
      </div>
  )
};

ReactDOM.render(<App />, document.querySelector('#root'));